<?php

declare(strict_types=1);

namespace App\Utils;

use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Request;
use App\AlertType;

class Utils
{
    public static function cleanFlashBag(FlashBag $flashBag): void
    {
        if(!$flashBag->has(AlertType::ACTIVATION)) {
            return;
        }
        $flashes = $flashBag->clear();
        unset($flashes[AlertType::ACTIVATION]);
        $flashBag->setAll($flashes);
        return;
    }

    // public static function addFlashMessage(Request $request):FlashBag{
    //     return
    // }
}
