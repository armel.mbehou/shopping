<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CategoryFIxtures extends AppFixtures
{
    public function load(ObjectManager $manager): void
    {

        $category = $this->createCategory($manager, 'Hitech');
        $this->createCategory($manager, 'Ordinateur', $category);
        $this->createCategory($manager, 'Accessoire', $category);

        $parent2 = $this->createCategory($manager, 'Mode');
        $this->createCategory($manager, 'Homme', $parent2);
        $this->createCategory($manager, 'Femme', $parent2);
        $this->createCategory($manager, 'Enfant', $parent2);
        $manager->flush();
    }

    private function createCategory(ObjectManager $manager, string $name, Category $parent = null): Category
    {
        $category = new Category();
        $category->setName($name)->setParent($parent);
        $manager->persist($category);
        $this->addReference(sprintf('cat-%d', ++$this->counter), $category);
        return $category;
    }
}
