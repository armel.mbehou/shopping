<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

abstract class AppFixtures extends Fixture
{
    protected const LOCAL = "fr_FR";
    protected Generator $facker;

    public function __construct(protected int $counter = 0)
    {
        $this->facker = Factory::create(self::LOCAL);
    }
}
