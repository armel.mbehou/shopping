<?php

namespace App\DataFixtures;

use App\Entity\Image;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

;

class ImageFixtures extends AppFixtures implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 100; $i++) {
            $image = new Image();
            $image->setName($this->facker->image(null, 640, 480))
                ->setProduct($this->getReference(sprintf("prod-%d", rand(1, 10)))) ;
            $manager->persist($image);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ProductFixtures::class,
        ];
    }
}
