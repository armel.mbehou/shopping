<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use function Symfony\Component\String\u;

class UserFixtures extends AppFixtures
{

    public function __construct(private UserPasswordHasherInterface $passwordHasher)
    {
        parent::__construct();
    }
    public function load(ObjectManager $manager): void
    {
        $this->createAdmin($manager);
        $this->createUser($manager, 10);

        $manager->flush();
    }

    private function createUser(ObjectManager $manager, int $userNumber): void
    {
        for($i = 0; $i < $userNumber; $i++) {
            $user = (new User())
            ->setLastname($this->facker->lastName)
            ->setFirstname($this->facker->firstName)
            ->setEmail($this->facker->email)
            ->setAddress($this->facker->streetAddress)
            ->setCity($this->facker->city)
            ->setZipcode(u($this->facker->postcode)->replace(' ', ''));
            $user->setPassword($this->passwordHasher->hashPassword($user, 'secret'));
            $manager->persist($user);
        }

    }

    private function createAdmin(ObjectManager $manager): void{
        $admin = (new User())
            ->setLastname("MBEHOU")
            ->setFirstname('Armel')
            ->setEmail('ambehou@demo.fr')
            ->setAddress('13 Rue Jeanne d\'arc')
            ->setCity('Massy')
            ->setRoles(['ROLE_ADMIN'])
            ->setZipcode('91300');
        $admin->setPassword($this->passwordHasher->hashPassword($admin, 'admin'));
        $manager->persist($admin);
    }
}
