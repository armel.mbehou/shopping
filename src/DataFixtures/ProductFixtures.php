<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends AppFixtures
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName($this->facker->text(30))
                ->setDescription($this->facker->text())
                ->setPrice($this->facker->numberBetween(10, 2000))
                ->setStock($this->facker->numberBetween(0, 10))
                ->setCategory($this->getReference(sprintf('cat-%d', rand(1, 7))))
            ;
            $manager->persist($product);
            $this->addReference(sprintf('prod-%d', ++$this->counter), $product);
            $manager->flush();
        }
    }
}
