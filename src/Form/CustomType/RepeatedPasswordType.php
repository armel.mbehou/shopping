<?php

namespace App\Form\CustomType;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\AbstractType;

class RepeatedPasswordType extends AbstractType
{
    public function getParent():string
    {
        return RepeatedType::class;
    }

    public function configureOptions(OptionsResolver $resolver) : void
    {
        $resolver->setDefaults([
            'type' => PasswordType::class,
            'require' => true,
            'constraints' => [new NotBlank()],
            'first_options' => [
                'label' => 'Password',
                'attr'=> ['class'=> 'form-control']
            ],
            'second_options' => [
                'label' => 'Confirm Password',
                'attr'=> ['class'=> 'form-control']
            ]
        ]);
    }
}
