<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/', name: 'main_')]
class MainController extends AbstractController
{
    public function __construct(
        private CategoryRepository $categoryRepository,
        private ProductRepository $productRepository
    ) {}

    #[Route('/', name: 'index')]
    public function index(): Response
    {
        $products = $this->productRepository->findAll();
        return $this->render('main/index.html.twig', compact('products'));
    }

    #[Route('/category/{slug}', name: 'category')]
    public function categoryIndex(Category $category)
    {
        return $this->render(
            'main/index.html.twig',
            ['products' => $category->getProducts(), 'catId' => $category->getId()]
        );
    }
}
