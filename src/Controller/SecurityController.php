<?php

namespace App\Controller;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\UserRepository;
use App\Form\CustomType\RepeatedPasswordType;
use App\Event\UserAccountEvent;
use App\AlertType;

class SecurityController extends AbstractController
{
    public function __construct (
        private UserRepository $userRepos,
        private EventDispatcherInterface $dispatcher
    ) {
    }
    #[Route(path: '/login', name: 'app_login')]
    public function login (AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser())
        {
            return $this->redirectToRoute('target_path');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render(
            'security/login.html.twig',
            ['last_username' => $lastUsername, 'error' => $error]
        );
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout (): void
    {
        throw new \LogicException(
            'This method can be blank - it will be intercepted by the logout key on your firewall.'
        );
    }

    #[Route(path: '/forgotten-password', name: 'forgotten_pass')]
    public function forgottenPassword (Request $request): Response
    {
        if ($this->isGranted('AUTHENTICATED_FULLY'))
        {
            $this->addFlash(
                AlertType::QUESTION,
                [
                    'title' => 'Password Forgotten ?',
                    'text'  => 'Reinite your password from your profile'
                ]
            );
            return $this->redirectToRoute('profile');
        }
        $form = $this->createForm(EmailType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $email = $form->getData();
            $user  = $this->userRepos->findOneBy(['email' => $email]);
            $event = new UserAccountEvent($user, $request);
            $this->dispatcher->dispatch(
                $event,
                UserAccountEvent::USER_FORGOTTEN_PASSWORD
            );
            return $this->redirectToRoute('main_index');
        }
        return $this->render(
            'security/forgotten_password.html.twig',
            compact('form')
        );
    }

    #[Route(path: '/reset_password/{token}', name: 'reset_password')]
    public function resetPassword (
        string $token,
        Request $request,
        UserPasswordHasherInterface $passwordHasher
    ) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $this->addFlash(AlertType::INFO, 'Reset Your Password from your profile page');
            return $this->redirectToRoute('profile_index');
        }
        if (empty($token) || !$user = $this->userRepos->findOneByResetToken($token))
        {
            $this->addFlash(AlertType::DANGER, 'Invalid password token !');
            return $this->redirectToRoute('main_index');
        }
        $form = $this->createForm(RepeatedPasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $user->setResetToken('');
            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $form->get('first')->getData()
            );
            $this->userRepos->upgradePassword($user, $hashedPassword);
            $this->addFlash(
                AlertType::SUCCESS,
                'Your password have been successfully reseted !'
            );
            return $this->redirectToRoute('app_login');
        }
        return $this->render('security/reset_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
