<?php

namespace App\Controller;

use Throwable;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Mailing\MailSender;
use App\Service\JWT\UserTokenManager;
use App\Security\UserAuthenticator;
use App\Form\RegistrationFormType;
use App\Event\UserAccountEvent;
use App\Entity\User;

class RegistrationController extends AbstractController
{
    public function __construct (
        private UserTokenManager $tokenManager,
        private EventDispatcherInterface $dispatcher
    ) {
    }

    #[Route('/signup', name: 'app_register')]
    public function register (
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        UserAuthenticatorInterface $userAuthenticator,
        UserAuthenticator $authenticator,
        EntityManagerInterface $entityManager,
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            $event = new UserAccountEvent($user, $request);
            $this->dispatcher->dispatch(
                $event,
                UserAccountEvent::USER_REGISTRATION_SUCCESS
            );

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }



    #[IsGranted('ROLE_USER')]
    #[Route('/activate/{token}', name: 'account_activation')]
    public function accountActivation (string $token, Request $request, EntityManagerInterface $em): Response
    {
        /** @var User */
        $user  = $this->getUser();
        $event = new UserAccountEvent($user, $request);
        try
        {
            $userIdentifier = $this->tokenManager->getUserFromToken($token);
            if ($user->getUserIdentifier() === $userIdentifier[ 'username' ])
            {
                $user->setVerified(TRUE);
                $em->persist($user);
                $em->flush();
            }
            $this->dispatcher->dispatch($event, UserAccountEvent::NAME);
        }
        catch ( JWTDecodeFailureException $th )
        {
            $this->dispatcher->dispatch($event->setThrowable($th), UserAccountEvent::USER_VERIFIED_FAILED);
        }
        catch ( Throwable $th )
        {
            $this->addFlash('danger', 'An error occured when trying to activate account');
        }

        return $this->redirectToRoute('main_index');
    }


    #[Route('/reactivate', name: 'account_activation_resend')]
    public function resendActivationLink (MailSender $mailSender, Request $req): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED');
        /** @var User */
        $user = $this->getUser();
        try
        {
            $token = $this->tokenManager->generateToken($user);
            $mailSender->send(
                'noreply@localhost.me',
                $user->getEmail(),
                'Activation compte SHOPPING',
                'register',
                compact('user', 'token')
            );
        }
        catch ( \Throwable $th )
        {
            $this->addFlash('danger', $th->getMessage());
        }

        $headers = $req->server->getHeaders();
        if ($headers)
        {
            return $this->redirect($headers[ 'REFERER' ]);
        }
        return $this->redirectToRoute('main_index');

    }

}
