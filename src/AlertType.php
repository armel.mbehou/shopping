<?php

declare(strict_types=1);

namespace App;

final class AlertType
{
    const SUCCESS = 'success';
    const WARNING = 'warning';
    const DANGER = 'danger';
    const INFO = 'info';
    const QUESTION = 'primary';
    const NOTE = 'dark';
    const ACTIVATION = 'warning';
}
