<?php

namespace App\Service\Slugger\EntityListener;

use App\Entity\Category;
use App\Entity\Product;
use App\Service\Slugger\SlugComputerInterface;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

#[AsEntityListener(event: Events::prePersist, entity: Category::class)]
#[AsEntityListener(event: Events::preUpdate, entity: Category::class)]
#[AsEntityListener(event: Events::prePersist, entity: Product::class)]
#[AsEntityListener(event: Events::preUpdate, entity: Product::class)]
/**
 * Summary of SlugComputerEntityListener
 */
class SlugComputerEntityListener
{
    /**
     * Summary of __construct
     * @param \Symfony\Component\String\Slugger\SluggerInterface $slugger
     */
    public function __construct(private SluggerInterface $slugger) {}

    /**
     * Summary of prePersist
     * @param \App\Entity\Category $category
     * @param \Doctrine\ORM\Event\PrePersistEventArgs $event
     * @return void
     */
    public function prePersist(SlugComputerInterface $entity, PrePersistEventArgs $event): void
    {
        $entity->computeSlug($this->slugger);
    }

    /**
     * Summary of preUpdate
     * @param \App\Entity\Category $category
     * @param \Doctrine\ORM\Event\PreUpdateEventArgs $event
     * @return void
     */
    public function preUpdate(SlugComputerInterface $entity, LifecycleEventArgs $event): void
    {
        $entity->computeSlug($this->slugger);
    }
}
