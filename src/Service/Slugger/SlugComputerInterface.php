<?php

namespace App\Service\Slugger;

use Symfony\Component\String\Slugger\SluggerInterface;

interface SlugComputerInterface
{
    public function computeSlug(SluggerInterface $slugger): void;
}
