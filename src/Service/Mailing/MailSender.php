<?php

namespace App\Service\Mailing;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class MailSender
{
    public function __construct(private MailerInterface $mailer) {}

    public function send(string $from, string $to, string $subject, string $template, array $context):void {
        $mail = (new TemplatedEmail)
            ->to($to)
            ->from($from)
            ->subject($subject)
            ->htmlTemplate(sprintf('emails/%s.html.twig', $template))
            ->context($context)
        ;

        $this->mailer->send($mail);
    }
}
