<?php

namespace App\Service\JWT;

use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserTokenManager
{
    public function __construct(private JWTTokenManagerInterface $jwtManager) {}

    public function generateToken(UserInterface $user): string
    {
        return $this->jwtManager->create($user);
    }

    public function getUserFromToken(string $token): array
    {
        $authenticatedToken = new JWTUserToken();
        $authenticatedToken->setRawToken($token);
        return $this->jwtManager->decode($authenticatedToken);
    }

}
