<?php

namespace App\Entity\Trait;

use Doctrine\ORM\Mapping as ORM;

trait CreatedAtTrait
{
    #[ORM\Column]
    /**
     * Summary of createdAt
     * @var
     */
    private ?\DateTimeImmutable $createdAt = null;

    /**
     * Summary of getCreatedAt
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    /**
     * Summary of setCreatedAt
     * @return void
     */
    public function setCreatedAt(): void
    {
        $this->createdAt = new \DateTimeImmutable();
    }

}
