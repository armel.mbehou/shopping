<?php

namespace App\EventSubscriber;

use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Utils\Utils;
use App\Service\Mailing\MailSender;
use App\Service\JWT\UserTokenManager;
use App\Event\UserAccountEvent;
use App\AlertType;

class AccountEventSubscriber implements EventSubscriberInterface
{
    protected const ACTIVATION_SUCCESS_MSG   =
        'Your account has been successfully activated!';
    protected const ACTIVATION_FAILURE_MSG   = 'An error occured';
    protected const REGISTRATION_SUCCESS_MSG = 'Registration success';

    public function __construct (
        private MailSender $mailSender,
        private UserTokenManager $tokenManager,
        private TokenGeneratorInterface $tokenGenerator,
        private EntityManagerInterface $em
    ) {
    }

    public function onUserVerifiedSuccess (UserAccountEvent $event): void
    {
        if (!$event->getUser()->isVerified())
        {
            return;
        }
        /** @var FlashBag */
        $flashBag = $event->getRequest()->getSession()->getBag('flashes');
        Utils::cleanFlashBag($flashBag);
        $flashBag->add(AlertType::SUCCESS, self::ACTIVATION_SUCCESS_MSG);
    }

    public function onUserVerifiedFailed (UserAccountEvent $event): void
    {
        if (!$th = $event->getThrowable())
        {
            return;
        }
        /** @var FlashBag */
        $flashBag = $event->getRequest()->getSession()->getBag('flashes');
        $flashBag->add(
            AlertType::DANGER,
            [
                'title' => self::ACTIVATION_FAILURE_MSG,
                'text'  => str_replace(' JWT', '', $th->getMessage())
            ]
        );
    }

    public function onUserRegistrationSuccess (UserAccountEvent $event): void
    {
        $user  = $event->getUser();
        $token = $this->tokenManager->generateToken($user);
        $this->mailSender->send(
            'noreply@localhost.me',
            $user->getEmail(),
            'Activation compte SHOPPING',
            'register',
            compact('user', 'token')
        );
        /** @var FlashBag */
        $flashBag = $event->getRequest()->getSession()->getBag('flashes');
        $flashBag->add(
            AlertType::SUCCESS,
            [
                'title' => self::REGISTRATION_SUCCESS_MSG,
                'text'  => 'An email have been sent to activate your account'
            ]
        );
    }

    /**
     * Generate token and send email with reset password link
     * @param \App\Event\UserAccountEvent $event
     * @return void
     */
    public function onUserForgottenPassword (UserAccountEvent $event): void
    {
        $user = $event->getUser();
        if (!$user)
        {
            return;
        }
        $token = $this->tokenGenerator->generateToken();
        $user->setResetToken($token);
        $this->em->flush();
        $this->mailSender->send(
            'noreply@demo.fr',
            $user->getEmail(),
            'Reset password',
            'forgotten_password',
            compact('user', 'token')
        );
        /** @var FlashBag */
        $flashBag = $event->getRequest()->getSession()->getBag('flashes');
        $flashBag->add(
            AlertType::INFO,
            [
                'title' => 'You ask for reset password !',
                'text'  => 'An email have been sent for Password reset.'
            ]
        );
    }

    public static function getSubscribedEvents (): array
    {
        return [
            UserAccountEvent::NAME                      => 'onUserVerifiedSuccess',
            UserAccountEvent::USER_VERIFIED_FAILED      => 'onUserVerifiedFailed',
            UserAccountEvent::USER_REGISTRATION_SUCCESS => 'onUserRegistrationSuccess',
            UserAccountEvent::USER_FORGOTTEN_PASSWORD   => 'onUserForgottenPassword'
        ];
    }
}
