<?php

namespace App\EventSubscriber;

use Twig\Environment;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Utils\Utils;
use App\Repository\CategoryRepository;
use App\Event\UserAccountEvent;
use App\Entity\User;
use App\AlertType;

class TwigEventSubscriber implements EventSubscriberInterface {
    public function __construct(
        private Environment $twig,
        private CategoryRepository $categoryRepository,
        private TokenStorageInterface $tokenStorage
    ) {
    }

    public static function getSubscribedEvents(): array {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    public function onKernelController(ControllerEvent $event): void {
        $this->twig->addGlobal('categories', $this->categoryRepository->findAll());

        $activationMessage = $this->getAccountActivationFlashMessage();
        if(!empty($activationMessage)) {
            /** @var FlashBag */
            $flashBag = $event->getRequest()->getSession()->getBag('flashes');
            Utils::cleanFlashBag($flashBag);
            if(!$flashBag->has(AlertType::ACTIVATION)) {
                $flashBag->add(AlertType::ACTIVATION, $activationMessage);
            }
        }
    }

    private function getAccountActivationFlashMessage(): ?array {
        /**@var User */
        $user = $this->tokenStorage->getToken()?->getUser();

        if(!$user || $user->isVerified()) {
            return null;
        }
        return [
            'title' => 'Finalize your registration!',
            'text' => 'Your account is not activated!',
            'link' => [
                'route' => 'account_activation_resend',
                'text' => 'Resend activation link ?'
            ]
        ];
    }
}
