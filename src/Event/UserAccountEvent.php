<?php

namespace App\Event;

use Throwable;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpFoundation\Request;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use App\Entity\User;

class UserAccountEvent extends RequestEvent
{
    public const NAME = 'user.verified_success';
    public const USER_VERIFIED_FAILED = 'user.verified_failure';
    public const USER_REGISTRATION_SUCCESS = 'user.registration_success';
    public const USER_FORGOTTEN_PASSWORD = 'user.password_forgotten';

    public function __construct(
        protected ?User $user,
        protected ?Request $request,
        protected ?JWTDecodeFailureException $throwable = null
    ) {
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * Get the value of throwable
     */
    public function getThrowable()
    {
        return $this->throwable;
    }

    /**
     * Set the value of throwable
     *
     * @return  self
     */
    public function setThrowable(JWTDecodeFailureException $throwable): self
    {
        $this->throwable = $throwable;

        return $this;
    }
}
