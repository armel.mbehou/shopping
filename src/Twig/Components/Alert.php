<?php

namespace App\Twig\Components;

use Symfony\UX\TwigComponent\Attribute\ExposeInTemplate;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;
use App\AlertType;

#[AsTwigComponent]
class Alert
{
    public ?string $title = null;
    public ?string $message = null;

    #[ExposeInTemplate(name: 'type', getter: 'getType')]
    private string $type;

    public bool $hasIcon = true;
    public bool $isDismissible = true;


    #[ExposeInTemplate('icon')]
    public function getIcon(): ?string
    {
        if(!$this->hasIcon) {
            return null;
        }
        switch ($this->getType()) {
            case AlertType::SUCCESS:
                return '<i class="bi bi-check-circle-fill"></i>';
            case AlertType::WARNING:
            case AlertType::ACTIVATION:
                return '<i class="bi bi-exclamation-circle-fill"></i>';
            case AlertType::DANGER:
                return '<i class="bi bi-x-circle-fill"></i>';
            case AlertType::INFO:
                return '<i class="bi bi-info-circle-fill"></i>';
            case AlertType::QUESTION:
                return '<i class="bi bi-question-circle-fill"></i>';
            case AlertType::NOTE:
                return '<i class="bi bi-plus-circle-fill"></i>';
            default:
                return null;
        }
    }

    /**
     * Get the value of type
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */
    public function setType(?string $type): self
    {
        $this->type = $type;
        return $this;
    }
}
